/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.util;

import com.dthielke.herochat.Herochat;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public final class Messaging {
    
    private static final Map<String, ChatColor> COLORS = new HashMap<>();
    private static boolean placeholderAPIEnabled;
    
    private Messaging() {}
    
    static {
        COLORS.put("black", ChatColor.BLACK);
        COLORS.put("0", ChatColor.BLACK);
        
        COLORS.put("dark blue", ChatColor.DARK_BLUE);
        COLORS.put("dark_blue", ChatColor.DARK_BLUE);
        COLORS.put("1", ChatColor.DARK_BLUE);
        
        COLORS.put("dark green", ChatColor.DARK_GREEN);
        COLORS.put("dark_green", ChatColor.DARK_GREEN);
        COLORS.put("2", ChatColor.DARK_GREEN);
        
        COLORS.put("dark aqua", ChatColor.DARK_AQUA);
        COLORS.put("dark_aqua", ChatColor.DARK_AQUA);
        COLORS.put("teal", ChatColor.DARK_AQUA);
        COLORS.put("3", ChatColor.DARK_AQUA);
        
        COLORS.put("dark red", ChatColor.DARK_RED);
        COLORS.put("dark_red", ChatColor.DARK_RED);
        COLORS.put("4", ChatColor.DARK_RED);
        
        COLORS.put("dark purple", ChatColor.DARK_PURPLE);
        COLORS.put("dark_purple", ChatColor.DARK_PURPLE);
        COLORS.put("purple", ChatColor.DARK_PURPLE);
        COLORS.put("5", ChatColor.DARK_PURPLE);
        
        COLORS.put("gold", ChatColor.GOLD);
        COLORS.put("orange", ChatColor.GOLD);
        COLORS.put("6", ChatColor.GOLD);
        
        COLORS.put("gray", ChatColor.GRAY);
        COLORS.put("grey", ChatColor.GRAY);
        COLORS.put("7", ChatColor.GRAY);
        
        COLORS.put("dark gray", ChatColor.DARK_GRAY);
        COLORS.put("dark_gray", ChatColor.DARK_GRAY);
        COLORS.put("dark grey", ChatColor.DARK_GRAY);
        COLORS.put("dark_grey", ChatColor.DARK_GRAY);
        COLORS.put("8", ChatColor.DARK_GRAY);
        
        COLORS.put("blue", ChatColor.BLUE);
        COLORS.put("9", ChatColor.BLUE);
        
        COLORS.put("bright green", ChatColor.GREEN);
        COLORS.put("bright_green", ChatColor.GREEN);
        COLORS.put("light green", ChatColor.GREEN);
        COLORS.put("light_green", ChatColor.GREEN);
        COLORS.put("lime", ChatColor.GREEN);
        COLORS.put("green", ChatColor.GREEN);
        COLORS.put("a", ChatColor.GREEN);
        
        COLORS.put("aqua", ChatColor.AQUA);
        COLORS.put("b", ChatColor.AQUA);
        
        COLORS.put("red", ChatColor.RED);
        COLORS.put("c", ChatColor.RED);
        
        COLORS.put("light purple", ChatColor.LIGHT_PURPLE);
        COLORS.put("light_purple", ChatColor.LIGHT_PURPLE);
        COLORS.put("pink", ChatColor.LIGHT_PURPLE);
        COLORS.put("d", ChatColor.LIGHT_PURPLE);
        
        COLORS.put("yellow", ChatColor.YELLOW);
        COLORS.put("e", ChatColor.YELLOW);
        
        COLORS.put("white", ChatColor.WHITE);
        COLORS.put("f", ChatColor.WHITE);
        
        COLORS.put("random", ChatColor.MAGIC);
        COLORS.put("magic", ChatColor.MAGIC);
        COLORS.put("k", ChatColor.MAGIC);
        
        COLORS.put("bold", ChatColor.BOLD);
        COLORS.put("l", ChatColor.BOLD);
        
        COLORS.put("strike", ChatColor.STRIKETHROUGH);
        COLORS.put("strikethrough", ChatColor.STRIKETHROUGH);
        COLORS.put("m", ChatColor.STRIKETHROUGH);
        
        COLORS.put("underline", ChatColor.UNDERLINE);
        COLORS.put("n", ChatColor.UNDERLINE);
        
        COLORS.put("italic", ChatColor.ITALIC);
        COLORS.put("o", ChatColor.ITALIC);
        
        COLORS.put("reset", ChatColor.RESET);
        COLORS.put("r", ChatColor.RESET);
    }
    
    static {
        placeholderAPIEnabled = (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null);
    }

    public static void broadcast(String msg, @NotNull Object... params) {
        Bukkit.getServer().broadcastMessage(parameterizeMessage(msg, params));
    }
    
    public static ChatColor parseColor(String input) {
        return COLORS.get(input.toLowerCase().replace("&", ""));
    }
    
    public static void send(CommandSender sender, String msg, @NotNull Object... params) {
        sender.sendMessage(parameterizeMessage(msg, params));
    }
    
    // PRIVATE UTIL
    
    private static String parameterizeMessage(String msg, @NotNull Object... params) {
        msg = ChatColor.YELLOW + msg;
        for (int i = 0; i < params.length; i++) {
            msg = msg.replace("$" + (i + 1), ChatColor.RESET + params[i].toString() + ChatColor.YELLOW);
        }
        return msg;
    }
    
    public static String formatBungeeTags(@NotNull String msg, boolean isBungee) {
        if (isBungee) {
            msg = msg.replace("{servername}", Herochat.getMessageHandler().getServerName());
            msg = msg.replace("{serveralias}", Herochat.getMessageHandler().getServerAlias());
        } {
            msg = msg.replace("{servername}", "");
            msg = msg.replace("{serveralias}", "");
        }
        return msg;
    }

    public static String formatPAPITags(@NotNull Player player, String msg) {
        if (placeholderAPIEnabled) msg = PlaceholderAPI.setPlaceholders(player, msg);
        return msg;
    }
}
