/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Set;
import java.util.UUID;

/**
 * A chat participant.
 */
public interface Chatter {
    
    boolean addChannel(@NotNull Channel channel, boolean announce, boolean flagUpdate);
    
    void attachStorage(ChatterStorage storage);
    
    Result canBan(@NotNull Channel channel);
    
    Result canColorMessages(@NotNull Channel channel, ChatColor color);
    
    Result canEmote(@NotNull Channel channel);
    
    Result canFocus(@NotNull Channel channel);
    
    Result canJoin(@NotNull Channel channel, @Nullable String password);
    
    Result canKick(@NotNull Channel channel);
    
    Result canLeave(@NotNull Channel channel);
    
    Result canModify(String setting, Channel channel);
    
    Result canMute(@NotNull Channel channel);
    
    Result canRemove(@NotNull Channel channel);
    
    Result canSpeak(@NotNull Channel channel);
    
    Result canViewInfo(@NotNull Channel channel);
    
    Result canIgnore(Chatter other);
    
    Channel getActiveChannel();
    
    String getAFKMessage();
    
    Set<Channel> getChannels();
    
    Set<UUID> getIgnores();
    
    Channel getLastActiveChannel();
    
    Channel getLastFocusableChannel();
    
    Chatter getLastPrivateMessageSource();
    
    String getName();
    
    UUID getUniqueId();
    
    Player getPlayer();
    
    ChatterStorage getStorage();
    
    boolean hasChannel(@NotNull Channel channel);
    
    boolean isAFK();
    
    boolean isIgnoring(Chatter other);
    
    boolean isIgnoring(UUID name);
    
    boolean isInRange(Chatter other, int distance);
    
    boolean isMuted();
    
    boolean isIgnoringPms();
    
    void setIgnoringPms(boolean ingoring, boolean flagUpdate);
    
    boolean removeChannel(@NotNull Channel channel, boolean announce, boolean flagUpdate);
    
    void setActiveChannel(@Nullable Channel channel, boolean announce, boolean flagUpdate);
    
    void setAFK(boolean afk);
    
    void setAFKMessage(String message);
    
    void setIgnore(UUID name, boolean ignore, boolean flagUpdate);
    
    void setLastPrivateMessageSource(Chatter chatter);
    
    void setMuted(boolean muted, boolean flagUpdate);
    
    boolean shouldAutoJoin(@NotNull Channel channel);
    
    boolean shouldForceJoin(@NotNull Channel channel);
    
    boolean shouldForceLeave(@NotNull Channel channel);
    
    void refocus();
    
    void disconnect();
    
    /**
     * Permissions of a chatter.
     */
    enum Permission {
        JOIN("join"),
        LEAVE("leave"),
        SPEAK("speak"),
        EMOTE("emote"),
        KICK("kick"),
        BAN("ban"),
        MUTE("mute"),
        REMOVE("remove"),
        COLOR("color.all"),
        INFO("info"),
        FOCUS("focus"),
        AUTOJOIN("autojoin"),
        FORCE_JOIN("force.join"),
        FORCE_LEAVE("force.leave"),
        MODIFY_NICK("modify.nick"),
        MODIFY_COLOR("modify.color"),
        MODIFY_DISTANCE("modify.distance"),
        MODIFY_FORMAT("modify.format"),
        MODIFY_SHORTCUT("modify.shortcut"),
        MODIFY_PASSWORD("modify.password"),
        MODIFY_VERBOSE("modify.verbose"),
        MODIFY_FOCUSABLE("modify.focusable"),
        MODIFY_CROSSWORLD("modify.crossworld"),
        MODIFY_CHATCOST("modify.chatcost"),
        BLACK("color.black"),
        DARK_BLUE("color.dark_blue"),
        DARK_GREEN("color.dark_green"),
        DARK_AQUA("color.dark_aqua"),
        DARK_RED("color.dark_red"),
        DARK_PURPLE("color.dark_purple"),
        GOLD("color.gold"),
        GRAY("color.gray"),
        DARK_GRAY("color.dark_gray"),
        BLUE("color.blue"),
        GREEN("color.green"),
        AQUA("color.aqua"),
        RED("color.red"),
        LIGHT_PURPLE("color.light_purple"),
        YELLOW("color.yellow"),
        WHITE("color.white"),
        MAGIC("color.magic"),
        BOLD("color.bold"),
        STRIKETHROUGH("color.strikethrough"),
        UNDERLINE("color.underline"),
        ITALIC("color.italic"),
        RESET("color.reset");
        
        private String name;
        
        private Permission(String name) {
            this.name = name;
        }
        
        public String form(@NotNull Channel channel) {
            return "herochat." + name + "." + channel.getName();
        }
        
        public String formAll() {
            return "herochat." + name + ".all";
        }
        
        public String formWildcard() {
            return "herochat." + name + ".*";
        }
        
        @Override
        public String toString() {
            return name;
        }
    }
    
    /**
     * Result of an action of a chatter.
     */
    enum Result {
        NO_PERMISSION(false),
        NO_CHANNEL(false),
        INVALID(false),
        BANNED(false),
        MUTED(false),
        ALLOWED(true),
        BAD_WORLD(false),
        BAD_PASSWORD(false),
        FAIL(false);
        
        private final boolean success;
        
        Result(boolean success) {
            this.success = success;
        }
        
        @Contract(pure = true)
        public boolean isSuccess() {
            return success;
        }
        
        @Contract(pure = true)
        public boolean isFailure() {
            return !success;
        }
        
    }
    
}
