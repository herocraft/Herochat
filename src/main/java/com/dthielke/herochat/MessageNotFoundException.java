package com.dthielke.herochat;

/**
 * Thrown when a message key could not be translated.
 *
 * @see com.dthielke.herochat.Herochat#getMessage(String)
 */
public class MessageNotFoundException extends Exception {
    
    private static final long serialVersionUID = -610345829513225073L;
    
    public MessageNotFoundException(String key) {
        super("Messages.properties is missing: " + key);
    }
    
}
