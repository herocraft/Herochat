/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

/**
 * A class which manages persistent storage of {@link Chatter}s.
 */
public interface ChatterStorage {
    
    /**
     * Updates the flags of a chatter.
     *
     * @param chatter the chatter
     */
    abstract void flagUpdate(@NotNull Chatter chatter);
    
    /**
     * Loads a chatter.
     *
     * @param name the name
     * @param uuid the unique id
     * @return the chatter
     */
    abstract Chatter load(@NotNull String name, @NotNull UUID uuid);
    
    /**
     * Removes a chatter from the storage.
     *
     * @param chatter the chatter
     */
    abstract void removeChatter(@NotNull Chatter chatter);
    
    /**
     * Updates the storage.
     */
    abstract void update();
    
    /**
     * Updates a chatter in the storage.
     *
     * @param chatter the chatter
     */
    abstract void update(@NotNull Chatter chatter);
    
}
