/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

import org.bukkit.ChatColor;

public class ChatLogFormatter extends Formatter {
    
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private final boolean keepColor;
    
    public ChatLogFormatter(boolean keepColor) {
        super();
        this.keepColor = keepColor;
    }
    
    @Override
    public String format(LogRecord record) {
        return DATE_FORMAT.format(new Date(record.getMillis()))
            + " "
            + (keepColor? record.getMessage() : ChatColor.stripColor(record.getMessage()))
            + '\n';
    }
    
}
