/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dthielke.herochat.MessageNotFoundException;
import com.dthielke.herochat.ChannelChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.util.Messaging;
import org.intellij.lang.annotations.RegExp;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class MessageHandler {
    
    private final static int TICKS_TO_MS = 50;
    private final SpamTracker spamTracker = new SpamTracker();
    
    private List<String> censors = new ArrayList<>();
    private boolean twitterStyleMessages = true;
    private String serverName = "";
    private String serverAlias = "";
    
    /**
     * Toggles support for twitter-style messages. This will make the handler convert messages of the format:
     * <blockquote>
     * {@code @<player> <message>}
     * </blockquote>
     * into commands of format:
     * <blockquote>
     * {@code ch msg <player> <message>}
     * </blockquote>
     *
     * @param flag whether twitter-style message support is to be enabled
     */
    public void setTwitterStyleMessages(boolean flag) {
        this.twitterStyleMessages = flag;
    }
    
    public void setServerName(String name) {
        this.serverName = name;
    }

    public void setServerAlias(String alias) {
        this.serverAlias = alias;
    }

    public String getServerName() {
        return serverName;
    }

    public String getServerAlias() {
        return serverAlias;
    }
    /**
     * Handles a player message.
     *
     * @param player the player
     * @param msg the message
     * @param format the format, usually passed down from {@link org.bukkit.event.player.AsyncPlayerChatEvent}
     */
    public void handle(@NotNull Player player, @NotNull String msg, @NotNull String format) {
        // If player has disconnected don't even bother sending the message.
        if (!player.isOnline()) {
            return;
        }
        
        /* apply global spam-filter
        if (!spamTracker.onChat(player.getUniqueId())) {
            try {
                Messaging.send(player, Herochat.getMessage("messageHandler_spam"));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
            return;
        }
        */
        
        // check if we have a @-prefixed private message
        if (twitterStyleMessages && msg.length() > 1 && msg.charAt(0) == '@' && msg.charAt(1) != ' ') {
            // CONVERT @<target> <msg> INTO COMMAND ch msg <target> <msg>
            msg = "msg " + msg.substring(1);
            Herochat.getCommandHandler().dispatch(player, "ch", msg.split(" "));
            return;
        }
        
        ChatterManager chatterManager = Herochat.getChatterManager();
        if (!chatterManager.hasChatter(player)) {
            chatterManager.addChatter(player);
        }
        
        Chatter sender = chatterManager.getChatter(player);
        if (sender == null) {
            throw new RuntimeException("Chatter (" + player.getName() + ") not found.");
        }
        
        
        Channel channel = sender.getActiveChannel();
        if (channel == null) {
            sendErrorMessage(player, Result.NO_CHANNEL, null);
            return;
        }
    
        if (!player.hasPermission("herochat.bypass.spam") && !channel.getSpamTracker().onChat(player.getUniqueId())) {
            try {
                Messaging.send(player, Herochat.getMessage("messageHandler_spam"));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
            return;
        }
    
        Result result;
        ChannelChatEvent channelEvent;
        
        // see if the player can speak in the active channel
        result = sender.canSpeak(channel);
        
        // throw a channel chat event
        channelEvent = throwChannelEvent(sender, channel, result, msg, format, channel.getFormat());
        result = channelEvent.getResult();
        channel = channelEvent.getChannel();
        
        if (!result.isSuccess()) {
            sendErrorMessage(player, result, channel);
            return;
        }
        
        // colorize the message
        Pattern pattern = Pattern.compile("(?i)(&)([0-9a-fk-or])");
        Matcher match = pattern.matcher(channelEvent.getMessage());
        StringBuffer sb = new StringBuffer();
        while (match.find()) {
            ChatColor color = ChatColor.getByChar(match.group(2).toLowerCase());
            if (sender.canColorMessages(channel, color) == Result.ALLOWED) {
                match.appendReplacement(sb, color.toString());
            }
            else {
                match.appendReplacement(sb, "");
            }
        }
        
        // create the string, and apply the censoring
        channelEvent.setMessage(censor(match.appendTail(sb).toString()));
        
        // pass it to the channel for additional processing
        channel.processChat(channelEvent);
    }
    
    private static void sendErrorMessage(Player player, Result result, Channel channel) {
        try {
            switch (result) {
                case NO_CHANNEL:
                    Messaging.send(player, Herochat.getMessage("messageHandler_noChannel"));
                    break;
                case INVALID:
                    Messaging.send(player, Herochat.getMessage("messageHandler_notInChannel"));
                    break;
                case MUTED:
                    Messaging.send(player, Herochat.getMessage("messageHandler_muted"));
                    break;
                case NO_PERMISSION:
                    Messaging.send(player, Herochat.getMessage("messageHandler_noPermission"),
                        channel.getColor() + channel.getName());
                    break;
                case BAD_WORLD:
                    Messaging.send(player, Herochat.getMessage("messageHandler_badWorld"),
                        channel.getColor() + channel.getName());
                    break;
            }
        } catch (MessageNotFoundException e) {
            Herochat.severe(e.getMessage());
        }
    }
    
    public static ChannelChatEvent throwChannelEvent(Chatter sender, Channel channel, Result result, String msg,
                                                     String bukkitFormat, String format) {
        ChannelChatEvent event = new ChannelChatEvent(sender, channel, result, msg, bukkitFormat, format);
        Bukkit.getPluginManager().callEvent(event);
        return event;
    }
    
    // ANTI-SPAM
    
    public SpamTracker getSpamTracker() {
        return spamTracker;
    }
    
    /**
     * Returns the default message limit for channels, before messages are caught by the spam filter.
     *
     * @return the default message limit
     */
    public int getDefaultMessageLimit() {
        return spamTracker.getLimit();
    }
    
    /**
     * Returns the amount of ticks between each message rate build-off.
     *
     * @return the amount of ticks between limit build-offs
     */
    public int getDefaultMessageLimitBuildOff() {
        return spamTracker.getBuildOff() / TICKS_TO_MS;
    }
    
    public void setDefaultMessageLimit(int limit) {
        spamTracker.setLimit(limit);
    }
    
    public void setDefaultMessageLimitBuildOff(int buildOffTicks) {
        spamTracker.setBuildOff(buildOffTicks * TICKS_TO_MS);
    }
    
    // CENSORSHIP
    
    /**
     * Sets the censor patterns of the handler.
     *
     * @param censors the censor patterns
     */
    public void setCensors(List<String> censors) {
        this.censors = new ArrayList<>(censors);
    }
    
    /**
     * Censors a message using all censorship patterns available to the {@link MessageHandler}.
     *
     * @param msg the input message
     * @return the censored message
     */
    public String censor(String msg) {
        for (String censorPatternAndReplacement : censors) {
            String[] split = censorPatternAndReplacement.split(";", 2);
            if (split.length == 1) {
                msg = replace(msg, split[0], null);
            }
            else {
                msg = replace(msg, split[0], split[1]);
            }
        }
        return msg;
    }
    
    // UTIL
    
    private static String replace(@NotNull String msg, @RegExp @NotNull String patternString,
                                  @Nullable String replacement) {
        Pattern pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(msg);
        StringBuilder censoredMsg = new StringBuilder();
        while (matcher.find()) {
            String match = matcher.group();
            if (replacement == null) {
                char[] replaceChars = new char[match.length()];
                Arrays.fill(replaceChars, '*');
                replacement = new String(replaceChars);
            }
            censoredMsg.append(msg.substring(0, matcher.start())).append(replacement);
            msg = msg.substring(matcher.end());
            matcher = pattern.matcher(msg);
        }
        censoredMsg.append(msg);
        
        return censoredMsg.toString();
    }
    
}
