/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import com.dthielke.herochat.MessageNotFoundException;
import com.dthielke.herochat.ChannelChatEvent;
import com.dthielke.herochat.ChatCompleteEvent;
import com.dthielke.herochat.util.Messaging;
import com.dthielke.herochat.util.UUIDConverter;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ConversationChannel extends StandardChannel {
    
    public ConversationChannel(Chatter memberOne, Chatter memberTwo, MessageFormatSupplier formatSupplier) {
        super(Herochat.getChannelManager().getStorage(),
            "convo" + memberOne.getName() + memberTwo.getName(),
            "convo" + memberTwo.getName() + memberOne.getName(),
            formatSupplier);
        super.addMember(memberOne, false, false);
        super.addMember(memberTwo, false, false);
        setFormat(formatSupplier.getConversationFormat());
    }
    
    @Override
    public boolean addMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate) {
        return getMembers().size() < 2 && super.addMember(chatter, false, false);
    }
    
    @Override
    public void addWorld(@NotNull String world) {}
    
    public String applyFormat(String format, Player sender, Player recipient) {
        // replace private message tags (from/to and sender/receiver)
        if (sender.equals(recipient)) {
            Player target = null;
            for (Chatter chatter : getMembers()) {
                if (!chatter.getPlayer().equals(sender)) {
                    target = chatter.getPlayer();
                    break;
                }
            }
            if (target != null) {
                format = format.replace("{convoaddress}", "To");
                format = format.replace("{convoplainpartner}", target.getName());
                format = format.replace("{convopartner}", target.getDisplayName());
            }
        }
        else {
            format = format.replace("{convoaddress}", "From");
            format = format.replace("{convoplainpartner}", sender.getName());
            format = format.replace("{convopartner}", sender.getDisplayName());
        }
        
        // custom tag support
        Pattern pattern = Pattern.compile("(\\{(\\w+)\\})");
        Matcher matcher = pattern.matcher(format);
        while (matcher.find()) {
            String tag = matcher.group();
            format.replace(tag, formatTag(tag.replace("{", "").replace("}", ""), sender, this));
        }
        
        format = Messaging.formatPAPITags(sender, format);
        format = format.replaceAll("(?i)&([0-9a-fk-or])", "\u00a7$1");
        return format;
    }
    
    @Override
    public boolean banMember(@NotNull Chatter chatter, boolean announce) {
        return false;
    }
    
    @NotNull
    @Override
    public Set<UUID> getBansUUID() {
        return new HashSet<UUID>();
    }

    @NotNull
    @Override
    public Set<String> getBans() {
        return UUIDConverter.uuidToString(getBansUUID());
    }
    
    @Override
    public int getDistance() {
        return 0;
    }
    
    @NotNull
    @Override
    public Set<UUID> getModeratorsUUID() {
        return new HashSet<UUID>();
    }
    
    @NotNull
    @Override
    public Set<String> getModerators() {
        return UUIDConverter.uuidToString(getModeratorsUUID());
    }
    
    @NotNull
    @Override
    public Set<UUID> getMutesUUID() {
        return new HashSet<UUID>();
    }
    
    @NotNull
    @Override
    public Set<String> getMutes() {
        return UUIDConverter.uuidToString(getMutesUUID());
    }
    
    @Nullable
    @Override
    public String getPassword() {
        return null;
    }
    
    @Override
    public Set<String> getWorlds() {
        return new HashSet<String>();
    }
    
    @Override
    public boolean hasWorld(@NotNull World world) {
        return true;
    }
    
    @Override
    public boolean isBanned(@NotNull UUID name) {
        return false;
    }
    
    @Override
    public boolean isHidden() {
        return true;
    }
    
    @Override
    public boolean isLocal() {
        return false;
    }
    
    @Override
    public boolean isModerator(@NotNull UUID name) {
        return false;
    }
    
    @Override
    public boolean isMuted(@NotNull UUID name) {
        return false;
    }
    
    @Override
    public boolean isShortcutAllowed() {
        return false;
    }
    
    @Override
    public boolean isTransient() {
        return true;
    }
    
    @Override
    public boolean kickMember(@NotNull Chatter chatter, boolean announce) {
        return false;
    }
    
    @Override
    public boolean isCrossServer() {
        return true;
    }

    @Override
    public void setCrossServer(boolean crossServer, boolean save) {}

    @Override
    public void onFocusLoss(@NotNull Chatter chatter) {
        // see if anyone has this channel active
        for (Chatter member : getMembers()) {
            if (member.getActiveChannel() != null && member.getActiveChannel().equals(this)) {
                return;
            }
        }
        
        // if not, remove all the members...
        for (Iterator<Chatter> iter = getMembers().iterator(); iter.hasNext(); ) {
            Chatter member = iter.next();
            iter.remove();
            member.removeChannel(this, false, true);
        }
        
        // ...and scrap the channel
        Herochat.getChannelManager().removeChannel(this);
    }
    
    @Override
    public void processChat(@NotNull ChannelChatEvent event) {
        Player player = event.getSender().getPlayer();
        String senderName = player.getName();
        UUID senderId = player.getUniqueId();
        Chatter sender = Herochat.getChatterManager().getChatter(player);
        
        String format = event.getFormat();
        // strip the BungeeCord tags from private messaging. It's not working over the bungee anyways
        format = Messaging.formatBungeeTags(format, false);
        for (Chatter member : getMembers()) {
            Player memberPlayer = member.getPlayer();
            // send the message
            if (!member.isIgnoring(Herochat.getChatterManager().getChatter(senderId))) {
                String appliedFormat = applyFormat(format, player, memberPlayer);
                memberPlayer.sendMessage(appliedFormat.replace("{msg}", event.getMessage()));
            }
            else {
                // Ignore chat sent from ignoring players
                return;
            }
            // send an automatic AFK response
            if (!sender.equals(member) && member.isAFK()) {
                String afkMsg = member.getAFKMessage();
                try {
                    afkMsg = afkMsg.isEmpty()? ("<AFK> " + Herochat.getMessage("convo_afk")) : ("<AFK> " + afkMsg);
                } catch (MessageNotFoundException e) {
                    Herochat.severe(e.getMessage());
                }
                player.sendMessage(applyFormat(format, memberPlayer, player).replace("{msg}", afkMsg));
            }
            // store the recipient's last private message sender
            if (!sender.equals(member)) {
                member.setLastPrivateMessageSource(sender);
                //Log the chat based on this recipient
                Herochat.logChat(senderName + " -> " + member.getName() + ": " + event.getMessage());
            }
        }
        Bukkit.getPluginManager().callEvent(new ChatCompleteEvent(sender, this, event.getMessage()));
        
    }
    
    @Override
    public boolean removeMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate) {
        // if we remove one member, the conversation is over and the channel should be scrapped
        if (super.removeMember(chatter, false, flagUpdate)) {
            int count = getMembers().size();
            if (count == 1) {
                // remove the remaining member
                Chatter otherMember = getMembers().iterator().next();
                removeMember(otherMember, false, flagUpdate);
                // change their active channel
                if (otherMember.getActiveChannel() != null && otherMember.getActiveChannel().equals(this)) {
                    otherMember.setActiveChannel(null, true, flagUpdate);
                }
                // scrap the channel
                Herochat.getChannelManager().removeChannel(this);
            }
            return true;
        }
        else {
            return false;
        }
    }
    
    @Override
    public void removeWorld(@NotNull String world) {}
    
    @Override
    public void setBanned(@NotNull UUID name, boolean banned) {}
    
    @Override
    public void setBansUUID(@NotNull Set<UUID> bans) {}
    
    @Override
    public void setBans(@NotNull Set<String> bans) {}
    
    @Override
    public void setModerator(@NotNull UUID name, boolean moderator) {}
    
    @Override
    public void setModeratorsUUID(@NotNull Set<UUID> moderators) {}

    @Override
    public void setModerators(@NotNull Set<String> moderators) {}
    
    @Override
    public void setMuted(@NotNull UUID name, boolean muted) {}
    
    @Override
    public void setMutesUUID(@NotNull Set<UUID> mutes) {}

    @Override
    public void setMutes(@NotNull Set<String> mutes) {}
    
    @Override
    public void setNick(@NotNull String nick) {}
    
    @Override
    public void setPassword(String password) {}
    
    @Override
    public void setShortcutAllowed(boolean shortcutAllowed) {}
    
    @Override
    public void setWorlds(@NotNull Set<String> worlds) {}
    
}
