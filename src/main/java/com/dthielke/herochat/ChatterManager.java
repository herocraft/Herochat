/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ChatterManager {
    
    private Map<UUID, Chatter> chatters = new HashMap<>();
    private ChatterStorage storage;
    
    public Chatter addChatter(Player player) {
        UUID id = player.getUniqueId();
        Chatter chatter = chatters.get(id);
        
        if (chatter == null) {
            chatter = storage.load(player.getName(), player.getUniqueId());
            if (chatter == null) {
                Herochat.severe("Null chatter for: " + player.getName() + " was detected, wiping all player info and" +
                    "attempting to load bogus chatter.");
                chatter = new Channel.StandardChatter(storage, player);
                chatters.put(id, chatter);
                storage.flagUpdate(chatter);
            }
            else chatters.put(id, chatter);
        }
        
        return chatter;
    }
    
    /**
     * Clears all chatters and removes the current storage.
     */
    public void clear() {
        chatters.clear();
        storage = null;
    }
    
    public boolean hasChatter(@NotNull Player player) {
        return chatters.containsKey(player.getUniqueId());
    }
    
    /**
     * Returns a chatter of the given player or {@link null} if none exists.
     *
     * @param player the player
     * @return the chatter of the player
     */
    public Chatter getChatter(@NotNull Player player) {
        return chatters.get(player.getUniqueId());
    }
    
    public Chatter getChatter(UUID id) {
        return chatters.get(id);
    }
    
    public Collection<Chatter> getChatters() {
        return chatters.values();
    }
    
    public ChatterStorage getStorage() {
        return storage;
    }
    
    public void removeChatter(@NotNull Chatter chatter) {
        storage.removeChatter(chatter);
        chatter.disconnect();
        chatters.remove(chatter.getUniqueId());
    }
    
    public void removeChatter(@NotNull Player player) {
        removeChatter(getChatter(player));
    }
    
    public void setStorage(@NotNull ChatterStorage storage) {
        this.storage = storage;
    }
    
    public void reset() {
        chatters.clear();
    }
    
}
