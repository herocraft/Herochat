/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import com.dthielke.herochat.MessageNotFoundException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dthielke.herochat.util.Messaging;

public class HCPlayerListener implements Listener {
    
    private final Herochat plugin;
    
    public HCPlayerListener(Herochat plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChat(final AsyncPlayerChatEvent event) {
        final Player player = event.getPlayer();
        final String msg = event.getMessage();
        final String format = event.getFormat();
        
        Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
            @Override
            public void run() {
                Herochat.getMessageHandler().handle(player, msg, format);
            }
        });
        event.setCancelled(true);
    }
    
    @EventHandler(ignoreCancelled = true)
    public void onPlayerCommandPreProcess(PlayerCommandPreprocessEvent event) {
        String input = event.getMessage().substring(1);
        String[] args = input.split(" ");
        Channel channel = Herochat.getChannelManager().getChannel(args[0]);
        if (channel != null && channel.isShortcutAllowed()) {
            event.setCancelled(true);
            Herochat.getCommandHandler().dispatch(event.getPlayer(), "ch qm", args);
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Chatter chatter = Herochat.getChatterManager().addChatter(event.getPlayer());
        if (chatter.isIgnoringPms()) {
            try {
                Messaging.send(event.getPlayer(), Herochat.getMessage("ignore_allPm"));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Herochat.getChatterManager().removeChatter(event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event) {
        Herochat.getChatterManager().getChatter(event.getPlayer()).refocus();
    }
    
}
