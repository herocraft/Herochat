/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

public class ConfigManager {
    
    /**
     * Loads the config from the given file and also saves it to the file while initializing all unset options with
     * default values.
     *
     * @param file the config file
     */
    public void load(File file) throws IOException {
        FileConfiguration config = new YamlConfiguration();
        try {
            if (file.exists())
                config.load(file);
        } catch (InvalidConfigurationException e) {
            throw new IOException(e);
        }
        config.setDefaults(getDefaults());
        
        ChannelManager channelManager = Herochat.getChannelManager();
        MessageHandler messageHandler = Herochat.getMessageHandler();
        
        ConfigurationSection modPermsSection = config.getConfigurationSection("moderator-permissions");
        if (modPermsSection != null) {
            if (modPermsSection.getBoolean("can-kick"))
                channelManager.addModPermission(Chatter.Permission.KICK);
            if (modPermsSection.getBoolean("can-ban"))
                channelManager.addModPermission(Chatter.Permission.BAN);
            if (modPermsSection.getBoolean("can-mute"))
                channelManager.addModPermission(Chatter.Permission.MUTE);
            if (modPermsSection.getBoolean("can-remove-channel"))
                channelManager.addModPermission(Chatter.Permission.REMOVE);
            if (modPermsSection.getBoolean("can-modify-nick"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_NICK);
            if (modPermsSection.getBoolean("can-modify-color"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_COLOR);
            if (modPermsSection.getBoolean("can-modify-distance"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_DISTANCE);
            if (modPermsSection.getBoolean("can-modify-password"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_PASSWORD);
            if (modPermsSection.getBoolean("can-modify-format"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_FORMAT);
            if (modPermsSection.getBoolean("can-modify-shortcut"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_SHORTCUT);
            if (modPermsSection.getBoolean("can-modify-verbose"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_VERBOSE);
            if (modPermsSection.getBoolean("can-modify-focusable"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_FOCUSABLE);
            if (modPermsSection.getBoolean("can-modify-crossworld"))
                channelManager.addModPermission(Chatter.Permission.MODIFY_CROSSWORLD);
            if (modPermsSection.getBoolean("can-color-messages"))
                channelManager.addModPermission(Chatter.Permission.COLOR);
            if (modPermsSection.getBoolean("can-view-info"))
                channelManager.addModPermission(Chatter.Permission.INFO);
            if (modPermsSection.getBoolean("can-focus"))
                channelManager.addModPermission(Chatter.Permission.FOCUS);
        }
        
        // load censors
        Herochat.getMessageHandler().setCensors(config.getStringList("censors"));
        
        // load anti-spam setting
        messageHandler.setDefaultMessageLimit(config.getInt("spam.message-limit"));
        messageHandler.setDefaultMessageLimitBuildOff(config.getInt("spam.message-limit-buildoff"));
        //System.out.println("IN CONFIG MANAGER: " + messageHandler.getSpamTracker());
        
        // load message formats
        channelManager.setStandardFormat(config.getString("format.default"));
        channelManager.setAnnounceFormat(config.getString("format.announce"));
        channelManager.setEmoteFormat(config.getString("format.emote"));
        channelManager.setConversationFormat(config.getString("format.private-message"));
        
        // load emote setting
        channelManager.setUsingEmotes(config.getBoolean("use-channel-emotes", true));
        
        // load locale
        try {
            Herochat.setLocale(loadLocale(config.getString("locale")));
        } catch (ClassNotFoundException e) {
            throw new IOException(e);
        }
    
        // load chat log setting
        Herochat.setChatLogEnabled(config.getBoolean("log-chat", true));
        
        // load bukkit logging setting
        Herochat.setLogToBukkitEnabled(config.getBoolean("log-to-bukkit", false));
        
        // load color logging setting
        Herochat.setColorLogging(config.getBoolean("log-colors", false));
        
        // load color logging setting for console log
        Herochat.setColorBukkitLogging(config.getBoolean("log-colors-to-bukkit", false));
        
        // load twitter style messages setting
        Herochat.getMessageHandler().setTwitterStyleMessages(config.getBoolean("twitter-style-private-messages", true));
        
        // load server alias & name
        Herochat.getMessageHandler().setServerName(config.getString("servername", ""));
        Herochat.getMessageHandler().setServerAlias(config.getString("serveralias", ""));
        
        // load cross-server support via bungee
        Herochat.setBungeeEnabled(config.getBoolean("use-cross-server", false));
        
        ConfigurationSection aliasSection = config.getConfigurationSection("world-alias");
        if (aliasSection != null) {
            for (String key : aliasSection.getKeys(false)) {
                channelManager.addWorldAlias(key, aliasSection.getString(key));
            }
        }
        
        // make sure we have at least one channel (create one if we don't)
        if (channelManager.getChannels().isEmpty()) {
            Channel global = new StandardChannel(channelManager.getStorage(), "Global", "G", channelManager);
            global.setColor(ChatColor.DARK_GREEN);
            channelManager.addChannel(global);
        }
    
        // load default channel
        String defaultChannel = config.getString("default-channel");
        if (defaultChannel != null && channelManager.hasChannel(defaultChannel)) {
            channelManager.setDefaultChannel(channelManager.getChannel(defaultChannel));
        }
    
        config.options().copyDefaults(true);
        config.save(file);
    }
    
    private Locale loadLocale(String locale) {
        if (locale.contains("_")) {
            int index = locale.indexOf("_");
            return new Locale(locale.substring(0, index), locale.substring(index + 1));
        }
        else if (locale.contains("-")) {
            int index = locale.indexOf("-");
            return new Locale(locale.substring(0, index), locale.substring(index + 1));
        }
        else return new Locale(locale);
    }
    
    private MemoryConfiguration getDefaults() {
        MemoryConfiguration config = new MemoryConfiguration();
        config.set("moderator-permissions.can-kick", true);
        config.set("moderator-permissions.can-ban", true);
        config.set("moderator-permissions.can-mute", true);
        config.set("moderator-permissions.can-remove-channel", true);
        config.set("moderator-permissions.can-modify-nick", true);
        config.set("moderator-permissions.can-modify-color", true);
        config.set("moderator-permissions.can-modify-distance", true);
        config.set("moderator-permissions.can-modify-password", true);
        config.set("moderator-permissions.can-modify-format", false);
        config.set("moderator-permissions.can-modify-shortcut", false);
        config.set("moderator-permissions.can-modify-verbose", true);
        config.set("moderator-permissions.can-modify-focusable", false);
        config.set("moderator-permissions.can-modify-crossworld", false);
        config.set("moderator-permissions.can-color-messages", true);
        config.set("moderator-permissions.can-view-info", true);
        config.set("moderator-permissions.can-focus", true);
        config.set("default-channel", "Global");
        config.set("censors", new ArrayList<String>());
        config.set("spam.message-limit", 3);
        config.set("spam.message-limit-buildoff", 20);
        config.set("format.default", Herochat.getChannelManager().getStandardFormat());
        config.set("format.announce", Herochat.getChannelManager().getAnnounceFormat());
        config.set("format.emote", Herochat.getChannelManager().getEmoteFormat());
        config.set("format.private-message", Herochat.getChannelManager().getConversationFormat());
        config.set("use-channel-emotes", true);
        config.set("locale", "en_US");
        config.set("log-chat", true);
        config.set("log-to-bukkit", false);
        config.set("log-colors", false);
        config.set("log-colors-to-bukkit", false);
        config.set("twitter-style-private-messages", true);
        config.set("use-cross-server", false);
        config.set("servername", "My Minecraft Server");
        config.set("serveralias", "[M]");
        config.set("servers", "");
        // save aliases
        for (String world : Herochat.getChannelManager().getWorldAliases().keySet()) {
            config.set("world-alias." + world, Herochat.getChannelManager().getWorldAlias(world));
        }
        return config;
    }
    
}
