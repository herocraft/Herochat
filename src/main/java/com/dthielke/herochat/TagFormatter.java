package com.dthielke.herochat;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public interface TagFormatter {

    @NotNull
    String formatTag(String tag, Player sender, Channel channel);
    
}
