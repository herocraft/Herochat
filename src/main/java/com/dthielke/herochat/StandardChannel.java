/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import com.dthielke.herochat.MessageNotFoundException;
import com.dthielke.herochat.ChannelChatEvent;
import com.dthielke.herochat.ChatCompleteEvent;
import com.dthielke.herochat.util.Messaging;
import com.dthielke.herochat.util.UUIDConverter;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StandardChannel implements Channel {
    
    private static final Pattern MSG_PATTERN = Pattern.compile("(.*)<(.*)%1\\$s(.*)> %2\\$s");
    private final static int TICKS_TO_MS = 50;
    
    private final String name;
    private String nick;
    private String format;
    private String password;
    private ChatColor color;
    private int distance;
    private boolean shortcutAllowed;
    private boolean verbose;
    private boolean crossWorld;
    private boolean crossServer = false;
    private boolean muted;
    private Set<Chatter> members = new HashSet<>();
    private Set<String> worlds = new HashSet<>();
    private Set<UUID> bans = new HashSet<>();
    private Set<UUID> mutes = new HashSet<>();
    private Set<UUID> moderators = new HashSet<>();
    private ChannelStorage storage;
    private final MessageFormatSupplier formatSupplier;
    private final Map<String, TagFormatter> tagFormatter = new HashMap<>();
    private final SpamTracker spamTracker;
    
    public StandardChannel(@NotNull ChannelStorage storage, @NotNull String name, @Nullable String nick,
                           @NotNull MessageFormatSupplier formatSupplier) {
        this.storage = storage;
        this.name = name;
        this.nick = nick;
        this.color = ChatColor.WHITE;
        this.distance = 0;
        this.shortcutAllowed = false;
        this.verbose = true;
        this.format = "{default}";
        this.password = null;
        this.formatSupplier = formatSupplier;
        this.muted = false;
        this.spamTracker = Herochat.getMessageHandler().getSpamTracker().clone();
    }
    
    // ACTIONS
    
    @Override
    public void processChat(@NotNull ChannelChatEvent event) {
        final Player player = event.getSender().getPlayer();
        
        String msg = applyFormat(event.getFormat(), event.getBukkitFormat(), player);
        msg = msg.replace("{sender}", player.getDisplayName());
        msg = msg.replace("{msg}", event.getMessage());
        
        Chatter sender = Herochat.getChatterManager().getChatter(player);
        Channel channel = sender.getActiveChannel();
        Collection<Player> recipients = new ArrayList<>(Bukkit.getOnlinePlayers());
        
        trimRecipients(recipients, sender);
        if (channel.isCrossServer() && Herochat.getPlugin().isBungeeEnabled()) {
            CrossServerListener.getInstance().sendChannelMessage(channel, Messaging.formatBungeeTags(msg, true));
        }

        msg = Messaging.formatBungeeTags(msg, false);
        for (Player recipient : recipients) {
            recipient.sendMessage(msg);
        }
        
        Bukkit.getPluginManager().callEvent(new ChatCompleteEvent(sender, this, msg));
        Herochat.logChat(msg);
    }
    
    @NotNull
    @SuppressWarnings("deprecation")
    @Override
    public String applyFormat(@NotNull String format, @NotNull String originalFormat, @NotNull Player sender) {
        format = applyFormat(format, originalFormat);
        format = format.replace("{plainsender}", sender.getName());
        format = format.replace("{sender}", sender.getDisplayName());
        // Allow aliasing for world replacement so that it doesn't just dump the world name.
        format = format.replace("{world}", sender.getWorld().getName());
        format = format.replace("{worldalias}", Herochat.getChannelManager().getWorldAlias(sender.getWorld().getName()));
        // Apply placeholders from PlaceholderAPI last
        format = Messaging.formatPAPITags(sender, format);
        Chat chat = Herochat.getChatService();
        if (chat != null) {
            try {
                String prefix = chat.getPlayerPrefix(sender);
                if (prefix == null || prefix.isEmpty()) {
                    prefix = chat.getPlayerPrefix((String) null, sender.getName());
                }
                String suffix = chat.getPlayerSuffix(sender);
                if (suffix == null || suffix.isEmpty()) {
                    suffix = chat.getPlayerSuffix((String) null, sender.getName());
                }
                String group = chat.getPrimaryGroup(sender);
                String groupPrefix = group == null? "" : chat.getGroupPrefix(sender.getWorld(), group);
                if (group != null && (groupPrefix == null || groupPrefix.isEmpty())) {
                    groupPrefix = chat.getGroupPrefix((String) null, group);
                }
                String groupSuffix = group == null? "" : chat.getGroupSuffix(sender.getWorld(), group);
                if (group != null && (groupSuffix == null || groupSuffix.isEmpty())) {
                    groupSuffix = chat.getGroupSuffix((String) null, group);
                }
                format = format.replace("{prefix}", prefix == null? "" : prefix.replace("%", "%%"));
                format = format.replace("{suffix}", suffix == null? "" : suffix.replace("%", "%%"));
                format = format.replace("{group}", group == null? "" : group.replace("%", "%%"));
                format = format.replace("{groupprefix}", groupPrefix == null? "" : groupPrefix.replace("%", "%%"));
                format = format.replace("{groupsuffix}", groupSuffix == null? "" : groupSuffix.replace("%", "%%"));
            } catch (UnsupportedOperationException ignored) {}
        }
        else for (String tag : new String[] {"{prefix}", "{suffix}", "{group}", "{groupprefix}", "{groupsuffix}"}) {
            format = format.replace(tag, "");
        }
        
        // Custom tag support
        Pattern pattern = Pattern.compile("(\\{(\\w+)})");
        Matcher matcher = pattern.matcher(format);
        while (matcher.find()) {
            String tag = matcher.group();
            format = format.replace(tag, formatTag(tag.replace("{", "").replace("}", ""), sender, this));
        }
        
        return format.replaceAll("(?i)&([a-fklmno0-9])", "\u00A7$1");
    }
    
    @NotNull
    @Override
    public String applyFormat(@NotNull String format, @NotNull String originalFormat) {
        format = format.replace("{default}", formatSupplier.getStandardFormat());
        format = format.replace("{name}", name);
        format = format.replace("{nick}", nick);
        format = format.replace("{color}", color.toString());

        return format.replaceAll("(?i)&([a-fklmnor0-9])", "\u00A7$1");
    }
    
    @Override
    public boolean kickMember(@NotNull Chatter chatter, boolean announce) {
        if (!members.contains(chatter)) {
            return false;
        }
        
        removeMember(chatter, false, true);
        
        if (announce) {
            try {
                announce(Herochat.getMessage("channel_kick").replace("$1", chatter.getPlayer().getDisplayName()));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
        }
        
        return true;
    }
    
    @Override
    public boolean removeMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate) {
        if (!members.contains(chatter)) {
            return false;
        }
        
        members.remove(chatter);
        if (chatter.hasChannel(this)) {
            chatter.removeChannel(this, announce, flagUpdate);
        }
        
        if (announce && verbose) {
            try {
                announce(Herochat.getMessage("channel_leave").replace("$1", chatter.getPlayer().getDisplayName()));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
        }
        
        return true;
    }
    
    // OTHER STUFF
    
    @Override
    public boolean addTag(@NotNull String tag, @NotNull TagFormatter formatter) {
        if (!tagFormatter.containsKey(tag)) {
            tagFormatter.put(tag, formatter);
            return true;
        }
        return false;
    }
    
    @NotNull
    @Override
    public String formatTag(String tag, Player sender, Channel channel) {
        TagFormatter formatter = tagFormatter.get(tag);
        if (formatter == null) {
            formatter = Herochat.getChannelManager().getTagFormatter(tag);
        }
        return formatter != null?
            formatter.formatTag(tag, sender, channel) :
            "{" + tag + "}";
    }
    
    @Override
    public boolean addMember(@NotNull Chatter chatter, boolean announce, boolean flagUpdate) {
        if (members.contains(chatter)) {
            return false;
        }
        
        if (announce && verbose) {
            try {
                announce(Herochat.getMessage("channel_join").replace("$1", chatter.getPlayer().getDisplayName()));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
        }
        
        members.add(chatter);
        if (!chatter.hasChannel(this)) {
            chatter.addChannel(this, announce, flagUpdate);
        }
        
        return true;
    }
    
    @Override
    public void addWorld(@NotNull String world) {
        if (!worlds.contains(world)) {
            worlds.add(world);
            storage.flagUpdate(this);
        }
    }
    
    @Override
    public void announce(@NotNull String message) {
        //handle colors first for announcements.
        String colorized = message.replaceAll("(?i)&([a-fklmno0-9])", "\u00A7$1");
        message = applyFormat(formatSupplier.getAnnounceFormat(), "").replace("{msg}", colorized);
        // strip the BungeeCord tags from announce
        message = Messaging.formatBungeeTags(message, false);
        for (Chatter member : members) {
            member.getPlayer().sendMessage(message);
        }
        Herochat.logChat(ChatColor.stripColor(message));
    }
    
    @Override
    public void sendRawMessage(@NotNull String message) {
        for (Chatter member : members)
            member.getPlayer().sendMessage(message);
    }
    
    @Override
    public void attachStorage(@NotNull ChannelStorage storage) {
        this.storage = storage;
    }
    
    @Override
    public boolean banMember(@NotNull Chatter chatter, boolean announce) {
        if (!members.contains(chatter)) {
            return false;
        }
        
        removeMember(chatter, false, true);
        setBanned(chatter.getPlayer().getUniqueId(), true);
        
        if (announce) {
            try {
                announce(Herochat.getMessage("channel_ban").replace("$1", chatter.getPlayer().getDisplayName()));
            } catch (MessageNotFoundException e) {
                Herochat.severe(e.getMessage());
            }
        }
        
        return true;
    }
    
    @Override
    public void emote(@NotNull Chatter sender, @NotNull String message) {
        message = applyFormat(formatSupplier.getEmoteFormat(), "", sender.getPlayer()).replace("{msg}", message);
        // strip the BungeeCord tags from emote
        message = Messaging.formatBungeeTags(message, false);
        // PlaceholderAPI
        message = Messaging.formatPAPITags(sender.getPlayer(), message);
        Set<Player> recipients = new HashSet<>();
        for (Chatter member : members) {
            recipients.add(member.getPlayer());
        }
        trimRecipients(recipients, sender);
        for (Player p : recipients) {
            p.sendMessage(message);
        }
        
        // Custom tag support
        Pattern pattern = Pattern.compile("(\\{(\\w+)})");
        Matcher matcher = pattern.matcher(format);
        while (matcher.find()) {
            String tag = matcher.group();
            format.replace(tag, formatTag(tag.replace("{", "").replace("}", ""), sender.getPlayer(), this));
        }
        
        Bukkit.getPluginManager().callEvent(new ChatCompleteEvent(sender, this, message));
        Herochat.logChat(message);
    }
    
    @NotNull
    @Override
    public Set<UUID> getBansUUID() {
        return new HashSet<>(bans);
    }
    
    @NotNull
    @Override
    public Set<String> getBans() {
        return UUIDConverter.uuidToString(getBansUUID());
    }
    
    @NotNull
    @Override
    public ChatColor getColor() {
        return color;
    }
    
    @Override
    public int getDistance() {
        return distance;
    }
    
    @NotNull
    @Override
    public String getFormat() {
        return format;
    }
    
    @NotNull
    @Override
    public Set<Chatter> getMembers() {
        return new HashSet<>(members);
    }
    
    @NotNull
    @Override
    public Set<UUID> getModeratorsUUID() {
        return new HashSet<>(moderators);
    }
    
    @NotNull
    @Override
    public Set<String> getModerators() {
        return UUIDConverter.uuidToString(getModeratorsUUID());
    }
    
    @NotNull
    @Override
    public Set<UUID> getMutesUUID() {
        return new HashSet<>(mutes);
    }
    
    @NotNull
    @Override
    public Set<String> getMutes() {
        return UUIDConverter.uuidToString(getMutesUUID());
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }
    
    @Nullable
    @Override
    public String getNick() {
        return nick;
    }
    
    @Nullable
    @Override
    public String getPassword() {
        return password;
    }
    
    @Override
    public ChannelStorage getStorage() {
        return storage;
    }
    
    @Override
    public Set<String> getWorlds() {
        return new HashSet<>(worlds);
    }
    
    @Override
    public SpamTracker getSpamTracker() {
        return spamTracker;
    }
    
    @Override
    public int getSpamMessageLimit() {
        return spamTracker.getLimit();
    }
    
    @Override
    public int getSpamMessageLimitBuildOff() {
        return spamTracker.getBuildOff() / TICKS_TO_MS;
    }
    
    @Override
    public boolean hasWorld(@NotNull String world) {
        return worlds.isEmpty() || worlds.contains(world);
    }
    
    @Override
    public boolean hasWorld(@NotNull World world) {
        return worlds.isEmpty() || worlds.contains(world.getName());
    }
    
    @Override
    public boolean isBanned(@NotNull UUID id) {
        return bans.contains(id);
    }
    
    @Override
    public boolean isCrossWorld() {
        return crossWorld;
    }
    
    @Override
    public boolean isHidden() {
        return false;
    }
    
    @Override
    public boolean isLocal() {
        return distance != 0;
    }
    
    @Override
    public boolean isMember(@NotNull Chatter chatter) {
        return members.contains(chatter);
    }
    
    @Override
    public boolean isModerator(@NotNull UUID id) {
        return moderators.contains(id);
    }
    
    @Override
    public boolean isMuted(@NotNull UUID id) {
        return muted || mutes.contains(id);
    }
    
    @Override
    public boolean isShortcutAllowed() {
        return shortcutAllowed;
    }
    
    @Override
    public boolean isTransient() {
        return false;
    }
    
    @Override
    public boolean isVerbose() {
        return verbose;
    }
    
    @Override
    public void onFocusGain(@NotNull Chatter chatter) {}
    
    @Override
    public void onFocusLoss(@NotNull Chatter chatter) {}
    
    @Override
    public void removeWorld(@NotNull String world) {
        if (worlds.contains(world)) {
            worlds.remove(world);
            storage.flagUpdate(this);
        }
    }
    
    @Override
    public void setBanned(@NotNull UUID id, boolean banned) {
        if (banned) {
            bans.add(id);
        }
        else {
            bans.remove(id);
        }
        storage.flagUpdate(this);
    }
    
    @Override
    public void setBansUUID(@NotNull Set<UUID> bans) {
        this.bans = bans;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setBans(@NotNull Set<String> bans) {
        setBansUUID(UUIDConverter.stringToUuid(bans));
    }
    
    @Override
    public void setColor(@NotNull ChatColor color) {
        this.color = color;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setCrossWorld(boolean crossWorld) {
        this.crossWorld = crossWorld;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setDistance(int distance) {
        this.distance = distance < 0? 0 : distance;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setFormat(@NotNull String format) {
        this.format = format;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setModerator(@NotNull UUID id, boolean moderator) {
        if (moderator) {
            moderators.add(id);
        }
        else {
            moderators.remove(id);
        }
        storage.flagUpdate(this);
    }
    
    @Override
    public void setModeratorsUUID(@NotNull Set<UUID> moderators) {
        this.moderators = moderators;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setModerators(@NotNull Set<String> moderators) {
        setModeratorsUUID(UUIDConverter.stringToUuid(moderators));
    }
    
    @Override
    public void setMuted(boolean value) {
        this.muted = value;
    }
    
    @Override
    public boolean isMuted() {
        return this.muted;
    }
    
    @Override
    public void setMuted(@NotNull UUID id, boolean muted) {
        if (muted) {
            mutes.add(id);
        }
        else {
            mutes.remove(id);
        }
        storage.flagUpdate(this);
    }
    
    @Override
    public void setMutesUUID(@NotNull Set<UUID> mutes) {
        this.mutes = mutes;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setMutes(@NotNull Set<String> mutes) {
        setMutesUUID(UUIDConverter.stringToUuid(mutes));
    }
    
    @Override
    public void setNick(@NotNull String nick) {
        this.nick = nick;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setPassword(String password) {
        this.password = password == null? "" : password;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setShortcutAllowed(boolean shortcutAllowed) {
        this.shortcutAllowed = shortcutAllowed;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
        storage.flagUpdate(this);
    }
    
    @Override
    public void setWorlds(@NotNull Set<String> worlds) {
        this.worlds = worlds;
        storage.flagUpdate(this);
    }
    
    @Override
    public MessageFormatSupplier getFormatSupplier() {
        return formatSupplier;
    }
    
    @Override
    public void setSpamMessageLimit(int limit) {
        this.spamTracker.setLimit(limit);
    }
    
    @Override
    public void setSpamMessageLimitBuildOff(int ticks) {
        this.spamTracker.setBuildOff(ticks * TICKS_TO_MS);
    }
    
    // MISC
    
    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        
        if (!(other instanceof Channel)) {
            return false;
        }
        
        Channel channel = (Channel) other;
        return name.equalsIgnoreCase(channel.getName()) || name.equalsIgnoreCase(channel.getNick());
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + name.toLowerCase().hashCode();
        result = prime * result + (nick == null? 0 : nick.toLowerCase().hashCode());
        return result;
    }
    
    // UTIL
    
    private boolean isMessageHeard(Set<Player> recipients, Chatter sender) {
        if (!isLocal()) return true;
        
        Player senderPlayer = sender.getPlayer();
        for (Player recipient : recipients)
            if (!recipient.hasPermission("herochat.admin.stealth") && !recipient.equals(senderPlayer))
                return true;
        
        return false;
    }
    
    private void trimRecipients(Collection<? extends Player> recipients, Chatter sender) {
        World world = sender.getPlayer().getWorld();
        for (Iterator<? extends Player> iterator = recipients.iterator(); iterator.hasNext(); ) {
            Chatter recipient = Herochat.getChatterManager().getChatter(iterator.next());
            if (recipient == null) {
                continue;
            }
            World recipientWorld = recipient.getPlayer().getWorld();
            if (!members.contains(recipient)
                || isLocal() && !sender.isInRange(recipient, distance)
                || !hasWorld(recipientWorld)
                || recipient.isIgnoring(sender)
                || !crossWorld && !world.equals(recipientWorld)) {
                iterator.remove();
            }
        }
    }
    
    // BUNGEECORD

    @Override
    public boolean isCrossServer() {
        return this.crossServer;
    }

    @Override
    public void setCrossServer(boolean crossServer, boolean save) {
        this.crossServer = crossServer;
        if (save && !this.isTransient()) {
            getStorage().flagUpdate(this);
        }
    }
}
