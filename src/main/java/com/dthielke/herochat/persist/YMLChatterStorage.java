/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.persist;

import com.dthielke.herochat.*;
import com.dthielke.herochat.util.UUIDConverter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.MemoryConfiguration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class YMLChatterStorage implements ChatterStorage {
    
    private Set<Chatter> updates = new HashSet<>();
    private final File chatterFolder;
    
    public YMLChatterStorage(File chatterFolder) {
        this.chatterFolder = chatterFolder;
    }
    
    @Override
    public void flagUpdate(@NotNull Chatter chatter) {
        updates.add(chatter);
    }
    
    @Override
    public Chatter load(@NotNull String name, @NotNull UUID id) {
        // only load a player if they're online
        Player player = Bukkit.getServer().getPlayer(id);
        if (player == null) {
            return null;
        }
        
        String idString = id.toString();
        File folder = new File(chatterFolder, name.substring(0, 1).toLowerCase());
        File file = new File(folder, name + "_" + idString + ".yml");
        FileConfiguration config = new YamlConfiguration();
        try {
            folder.mkdirs();
            if (file.exists()) {
                config.load(file);
            }
            else {
                File newFile = UUIDConverter.uuidChange(name, idString, chatterFolder, file);
                Bukkit.getLogger().info("NEW FILE NAME1");
                if (newFile != null) {
                    config.load(newFile);
                    Bukkit.getLogger().info("NEW FILE NAME");
                }
                else {
                    newFile = UUIDConverter.fileConvertToUuidFormat(name, idString, folder, file);
                    if (newFile != null) {
                        config.load(newFile);
                        Bukkit.getLogger().info("NEW FILE UUID");
                    }
                    Bukkit.getLogger().info("NEW FILE UUID2");
                }
            }
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
        
        Chatter chatter = new Channel.StandardChatter(this, player);
        chatter.setIgnoringPms(config.getBoolean("ignorepms"), false);
        loadChannels(chatter, config);
        loadActiveChannel(chatter, config);
        loadIgnores(chatter, config, file);
        loadMuted(chatter, config);
        return chatter;
    }
    
    @Override
    public void removeChatter(@NotNull Chatter chatter) {
        update(chatter);
        updates.remove(chatter);
    }
    
    @Override
    public void update() {
        if (!updates.isEmpty()) {
            Herochat.info("Saving players ....");
            Iterator<Chatter> iter = updates.iterator();
            while (iter.hasNext()) {
                Chatter chatter = iter.next();
                update(chatter);
                iter.remove();
            }
            Herochat.info("Save complete");
        }
    }
    
    @Override
    public void update(@NotNull Chatter chatter) {
        FileConfiguration config = new YamlConfiguration();
        String name = chatter.getName();
        String idString = UUIDConverter.uuidToString(chatter.getUniqueId());
        config.set("name", name);
        config.set("UUID", idString);
        if (chatter.getActiveChannel() != null) {
            if (chatter.getActiveChannel().isTransient()) {
                config.set("activeChannel", chatter.getLastActiveChannel().getName());
            }
            else {
                config.set("activeChannel", chatter.getActiveChannel().getName());
            }
        }
        config.set("ignorepms", chatter.isIgnoringPms());
        List<String> channels = new ArrayList<>();
        for (Channel channel : chatter.getChannels()) {
            if (!channel.isTransient()) {
                channels.add(channel.getName());
            }
        }
        config.set("channels", channels);
        config.set("ignores", new ArrayList<>(UUIDConverter.uuidToString(chatter.getIgnores())));
        config.set("muted", chatter.isMuted());
        config.set("autojoin", false);
        File folder = new File(chatterFolder, name.substring(0, 1).toLowerCase());
        File file = new File(folder, name + "_" + idString + ".yml");
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private void loadActiveChannel(Chatter chatter, MemoryConfiguration config) {
        ChannelManager channelManager = Herochat.getChannelManager();
        Channel defaultChannel = channelManager.getDefaultChannel();
        Channel activeChannel = channelManager.getChannel(config.getString("activeChannel", ""));
        if (activeChannel == null || !chatter.hasChannel(activeChannel)) {
            activeChannel = defaultChannel;
        }
        chatter.setActiveChannel(defaultChannel, false, false);
        chatter.setActiveChannel(activeChannel, false, false);
    }
    
    private void loadChannels(Chatter chatter, MemoryConfiguration config) {
        ChannelManager channelManager = Herochat.getChannelManager();
        Set<Channel> channels = new HashSet<>();
        
        // set the default list as an empty list
        config.addDefault("channels", new ArrayList<String>());
        
        // load previously joined channels
        List<String> channelNames = config.getStringList("channels");
        for (String channelName : channelNames) {
            Channel channel = channelManager.getChannel(channelName);
            if (channel != null && chatter.canJoin(channel, channel.getPassword()) == Chatter.Result.ALLOWED) {
                channels.add(channel);
            }
        }
        
        // add auto and forced join channels
        boolean autojoin = config.getBoolean("autojoin", true);
        for (Channel channel : channelManager.getChannels()) {
            if ((autojoin && chatter.shouldAutoJoin(channel)) || chatter.shouldForceJoin(channel)) {
                channels.add(channel);
            }
        }
        
        // remove forced leave channels
        for (Iterator<Channel> iterator = channels.iterator(); iterator.hasNext(); ) {
            if (chatter.shouldForceLeave(iterator.next())) {
                iterator.remove();
            }
        }
        
        // make sure we have at least one channel to join
        if (channels.isEmpty())
            channels.add(channelManager.getDefaultChannel());
        
        // join the channels
        for (Channel channel : channels)
            channel.addMember(chatter, false, false);
    }
    
    private void loadIgnores(Chatter chatter, FileConfiguration config, File file) {
        config.addDefault("ignores", new ArrayList<String>());
        Set<UUID> ignores = UUIDConverter.stringToUuid(config.getStringList("ignores"));
        config.set("ignores", UUIDConverter.uuidToString(ignores));
        try {
            config.save(file);
        } catch (IOException e) {
            Herochat.severe("Could not save file " + file.getName());
            e.printStackTrace();
        }
        for (UUID id : ignores) {
            chatter.setIgnore(id, true, false);
        }
    }
    
    private void loadMuted(Chatter chatter, MemoryConfiguration config) {
        boolean muted = config.getBoolean("muted", false);
        chatter.setMuted(muted, false);
    }
    
}
