/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.jetbrains.annotations.NotNull;

public interface MessageFormatSupplier {
    
    @NotNull
    String getStandardFormat();
    
    @NotNull
    String getConversationFormat();
    
    @NotNull
    String getAnnounceFormat();
    
    @NotNull
    String getEmoteFormat();
    
}
