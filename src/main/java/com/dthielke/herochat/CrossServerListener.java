/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Iterables;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class CrossServerListener implements PluginMessageListener {

    private static CrossServerListener instance;
    public static CrossServerListener getInstance() {
        if (instance == null) {
            throw new IllegalStateException("There is a problem with your configuration, attempted to send cross-server chat but your server is not setup for it.");
        }
        return instance;
    }
    private Map<String, Set<String>> players = new HashMap<String, Set<String>>();
    private final Set<String> servers = new HashSet<String>();
    private static final String chatChannel = "Herochat";

    public CrossServerListener(Collection<String> serverList) {
        if (instance != null) {
            throw new IllegalStateException("CrossServerListener is already initialized, only one may be constructed.");
        }
        instance = this;
        // Servers to send messages to.
        this.servers.addAll(serverList);
        Bukkit.getMessenger().registerIncomingPluginChannel(Herochat.getPlugin(), "BungeeCord", this);
        Bukkit.getMessenger().registerOutgoingPluginChannel(Herochat.getPlugin(), "BungeeCord");
        Bukkit.getScheduler().runTaskTimerAsynchronously(Herochat.getPlugin(), new Runnable() {

            @Override
            public void run() {
                // Check list of players periodically so we can forward PMs properly.
                for (String server : servers) {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("PlayerList");
                    out.writeUTF(server);
                    Player pl = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
                    if (pl != null) {
                        pl.sendPluginMessage(Herochat.getPlugin(), "BungeeCord", out.toByteArray());
                    }

                }
            }}, 20, 200);
    }

    @Override
    public void onPluginMessageReceived(String pluginChannel, Player player, byte[] bytes) {
        if(!pluginChannel.equalsIgnoreCase("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(bytes);

        String subChannel = in.readUTF();
        // Check if the subchannel is for herochat
        switch(subChannel) {
        case chatChannel:
            byte[] msgBytes = new byte[in.readShort()];
            in.readFully(msgBytes);
            receiveMessage(ByteStreams.newDataInput(msgBytes));
            break;
        case "PlayerList":
            String server = in.readUTF();
            if (server.equals("ALL")) {
                return; // We ask for specific servers, not ALL, but something else may request it. Make sure to filter that out.
            }
            Set<String> playerNames;
            if (!players.containsKey(server)) {
                playerNames = new HashSet<String>();
                players.put(server, playerNames);
            } else {
                playerNames = players.get(server);
                playerNames.clear();
            }
            for (String name : in.readUTF().split(", ")) {
                playerNames.add(name);
            }
            break;
        default:
            return;
        }

    }

    // Forward a given byte message to all servers
    private void transmitMessage(byte[] data) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Forward");
        out.writeUTF("ALL");
        out.writeUTF(chatChannel);
        out.writeShort(data.length);
        out.write(data);
        Player pl = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
        if (pl != null) {
            pl.sendPluginMessage(Herochat.getPlugin(), "BungeeCord", out.toByteArray());
        }
    }

    public void sendChannelMessage(Channel channel, String message) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("CHAT");
        out.writeUTF(channel.getName());
        out.writeUTF(message);
        transmitMessage(out.toByteArray());
    }

    private void receiveMessage(ByteArrayDataInput in) {
        String messageType = in.readUTF(); // type of message
        String channelName = in.readUTF(); // Channel this message is being sent to
        switch (messageType) {
        case "CHAT":
            Channel channel = Herochat.getChannelManager().getChannel(channelName);
            if (channel == null || !channel.isCrossServer()) {
                return;
            } else {
                channel.sendRawMessage(in.readUTF());
            }
            break;
        case "PM":
            break;
        case "PMRESPONSE":
            
        default:
            return;
        }

    }

}
