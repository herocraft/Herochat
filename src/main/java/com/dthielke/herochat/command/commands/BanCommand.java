/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;

import java.util.UUID;

public class BanCommand extends BasicCommand {
    
    public BanCommand() {
        super("Ban");
        setDescription(getMessage("command_ban"));
        setUsage("/ch ban " + ChatColor.DARK_GRAY + "[channel] <player>");
        setArgumentRange(1, 2);
        setIdentifiers("ch ban", "herochat ban");
        setNotes("\u00a7cNote:\u00a7e If no channel is given, your active", "      channel is used.");
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        Channel channel;
        Chatter chatter = null;
        
        if (sender instanceof Player) {
            Player player = (Player) sender;
            chatter = Herochat.getChatterManager().getChatter(player);
        }
        
        if (args.length == 1) {
            if (chatter != null) {
                channel = chatter.getActiveChannel();
            }
            else {
                channel = Herochat.getChannelManager().getDefaultChannel();
            }
        }
        else {
            channel = Herochat.getChannelManager().getChannel(args[0]);
        }
        
        if (channel == null) {
            Messaging.send(sender, getMessage("ban_noChannel"));
            return true;
        }
        
        if (chatter != null && chatter.canBan(channel) != Result.ALLOWED) {
            Messaging.send(sender, getMessage("ban_noPermission"), channel.getColor() + channel.getName());
            return true;
        }
        
        String targetName = args[args.length - 1];
        OfflinePlayer targetOfflinePlayer = Bukkit.getServer().getOfflinePlayer(targetName);
        Player targetPlayer = null;
        UUID targetId;
        if (targetOfflinePlayer != null) {
            targetId = targetOfflinePlayer.getUniqueId();
        }
        else return false;
        
        if (targetOfflinePlayer.isOnline()) {
            targetPlayer = Bukkit.getPlayer(targetId);
        }
        
        if (channel.isBanned(targetId)) {
            channel.setBanned(targetId, false);
            Messaging.send(sender, getMessage("ban_confirmUnban"), targetName, channel.getColor() + channel.getName());
            if (targetPlayer != null) {
                Messaging.send(targetPlayer, getMessage("ban_notifyUnban"), channel.getColor() + channel.getName());
            }
        }
        else {
            if (targetPlayer != null) {
                Chatter target = Herochat.getChatterManager().getChatter(targetPlayer);
                channel.banMember(target, true);
                
                if (target.getChannels().isEmpty()) {
                    Herochat.getChannelManager().getDefaultChannel().addMember(target, true, true);
                }
                
                if (channel.equals(target.getActiveChannel())) {
                    Channel focus = target.getChannels().iterator().next();
                    target.setActiveChannel(focus, true, true);
                }
            }
            else {
                channel.setBanned(targetId, true);
            }
            Messaging.send(sender, getMessage("ban_confirmBan"), targetName, channel.getColor() + channel.getName());
        }
        
        return true;
    }
    
}
