/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.ChannelManager;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinCommand extends BasicCommand {
    
    public JoinCommand() {
        super("Join");
        setDescription(getMessage("command_join"));
        setUsage("/ch join " + ChatColor.DARK_GRAY + "<channel> [password]");
        setArgumentRange(1, 2);
        setIdentifiers("join", "ch join", "herochat join");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        if (!(sender instanceof Player)) return true;
        
        Player player = (Player) sender;
        
        ChannelManager channelMngr = Herochat.getChannelManager();
        Channel channel = channelMngr.getChannel(args[0]);
        if (channel == null) {
            Messaging.send(sender, getMessage("join_noChannel"));
            return true;
        }
        
        String password = "";
        if (args.length == 2) {
            password = args[1];
        }
        
        Chatter chatter = Herochat.getChatterManager().getChatter(player);
        Result result = chatter.canJoin(channel, password);
        switch (result) {
            case INVALID:
                Messaging.send(sender, getMessage("join_redundant"), channel.getColor() + channel.getName());
                return true;
            case NO_PERMISSION:
                Messaging.send(sender, getMessage("join_noPermission"), channel.getColor() + channel.getName());
                return true;
            case BANNED:
                Messaging.send(sender, getMessage("join_banned"), channel.getColor() + channel.getName());
                return true;
            case BAD_PASSWORD:
                Messaging.send(sender, getMessage("join_badPassword"));
                return true;
            default:
        }
        
        channel.addMember(chatter, true, true);
        Messaging.send(player, getMessage("join_confirm"), channel.getColor() + channel.getName());
        return true;
    }
    
}
