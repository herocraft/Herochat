/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.command.Command;
import com.dthielke.herochat.command.CommandHandler;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

public class HelpCommand extends BasicCommand {
    
    private static final String
        HEADER = ChatColor.RED + "-----[ " + ChatColor.RESET + "Herochat Help <%d/%d>" + ChatColor.RED + " ]-----",
        INDENT = "  " + ChatColor.GREEN;
    
    private static final int COMMANDS_PER_PAGE = 8;
    
    public HelpCommand() {
        super("Help");
        setDescription(getMessage("command_help"));
        setUsage("/ch help " + ChatColor.DARK_GRAY + "[page#]");
        setArgumentRange(0, 1);
        setIdentifiers("ch help", "herochat help");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        int page = 0;
        if (args.length != 0) {
            try {
                page = Integer.parseInt(args[0]) - 1;
            } catch (NumberFormatException ignored) {}
        }
        
        List<Command> sortCommands = Herochat.getCommandHandler().getCommands();
        List<Command> commands = new ArrayList<>();
        
        // Filter out Skills from the command list.
        for (Command command : sortCommands) {
            if (command.isShownOnHelpMenu()) {
                if (CommandHandler.hasPermission(sender, command.getPermission())) {
                    commands.add(command);
                }
            }
        }
        
        int numPages = commands.size() / COMMANDS_PER_PAGE;
        if (commands.size() % COMMANDS_PER_PAGE != 0) {
            numPages++;
        }
        if (numPages == 0) {
            numPages = 1;
        }
        if (page >= numPages || page < 0) {
            page = 0;
        }
        sender.sendMessage(String.format(HEADER, page+1, numPages));
        int start = page * COMMANDS_PER_PAGE;
        int end = Math.min(start + COMMANDS_PER_PAGE, commands.size());
        
        for (int i = start; i < end; i++) {
            Command cmd = commands.get(i);
            sender.sendMessage(INDENT + cmd.getUsage());
        }
        
        Messaging.send(sender, getMessage("help_moreInfo"), getMessage("help_infoCommand"));
        return true;
    }
    
}
