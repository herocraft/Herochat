/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class ModCommand extends BasicCommand {
    
    public ModCommand() {
        super("Mod");
        setDescription(getMessage("command_mod"));
        setUsage("/ch mod " + ChatColor.DARK_GRAY + "[channel] <player>");
        setArgumentRange(1, 2);
        setIdentifiers("ch mod", "herochat mod");
        setNotes("\u00a7cNote:\u00a7e If no channel is given, your active", "      channel is used.");
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        Channel channel = null;
        Chatter chatter = null;
        
        if (sender instanceof Player) {
            Player player = (Player) sender;
            chatter = Herochat.getChatterManager().getChatter(player);
            channel = chatter.getActiveChannel();
        }
        
        if (args.length == 1) {
            if (chatter != null) {
                channel = chatter.getActiveChannel();
            }
        }
        else {
            channel = Herochat.getChannelManager().getChannel(args[0]);
        }
        
        if (channel == null) {
            Messaging.send(sender, getMessage("mod_noChannel"));
            return true;
        }
        
        if (!(sender.hasPermission("herochat.mod") || (chatter != null && channel.isModerator(chatter.getPlayer().getUniqueId())))) {
            Messaging.send(sender, getMessage("mod_noPermission"), channel.getColor() + channel.getName());
            return true;
        }
        
        String targetName = args[args.length - 1];
        OfflinePlayer targetOfflinePlayer = Bukkit.getServer().getOfflinePlayer(targetName);
        Player targetPlayer = null;
        UUID targetId;
        if (targetOfflinePlayer != null) {
            targetId = targetOfflinePlayer.getUniqueId();
        }
        else return false;
        
        if (targetOfflinePlayer.isOnline()) {
            targetPlayer = Bukkit.getPlayer(targetId);
        }
        if (channel.isModerator(targetId)) {
            channel.setModerator(targetId, false);
            Messaging.send(sender, getMessage("mod_confirmUnmod"), channel.getColor() + channel.getName(), targetName);
            if (targetPlayer != null) {
                Messaging.send(targetPlayer, getMessage("mod_notifyUnmod"), channel.getColor() + channel.getName());
            }
        }
        else {
            channel.setModerator(targetId, true);
            Messaging.send(sender, getMessage("mod_confirmMod"), channel.getColor() + channel.getName(), targetName);
            if (targetPlayer != null) {
                Messaging.send(targetPlayer, getMessage("mod_notifyMod"), channel.getColor() + channel.getName());
            }
        }
        
        return true;
    }
    
}
