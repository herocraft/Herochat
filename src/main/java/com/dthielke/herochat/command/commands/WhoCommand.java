/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;

public class WhoCommand extends BasicCommand {
    
    public WhoCommand() {
        super("Who");
        setDescription(getMessage("command_who"));
        setUsage("/ch who " + ChatColor.DARK_GRAY + "[channel]");
        setArgumentRange(0, 1);
        setIdentifiers("ch who", "herochat who");
        setNotes(ChatColor.RED + "Note:" + ChatColor.YELLOW + " If no channel is given, your active", "      channel is"
            + " used.");
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        Channel channel;
        Chatter chatter = null;
        
        if (sender instanceof Player) {
            Player player = (Player) sender;
            chatter = Herochat.getChatterManager().getChatter(player);
        }
        
        if (args.length == 0) {
            channel = chatter != null? chatter.getActiveChannel() : Herochat.getChannelManager().getDefaultChannel();
        }
        else {
            channel = Herochat.getChannelManager().getChannel(args[0]);
        }
        
        if (channel == null) {
            Messaging.send(sender, getMessage("who_noChannel"));
            return true;
        }
        
        List<String> names = new ArrayList<>();
        for (Chatter member : channel.getMembers()) {
            if (sender instanceof Player && !((Player) sender).canSee(member.getPlayer())) {
                continue;
            }
            names.add(member.getPlayer().getName());
        }
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });
        
        sender.sendMessage(ChatColor.RED + "------------[ " + channel.getColor() + channel.getName() + ChatColor.RED +
            " ]------------");
        int count = names.size();
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < count; i++) {
            String name = names.get(i);
            // color names for mods and muted players
            
            UUID targetId = Bukkit.getServer().getOfflinePlayer(name).getUniqueId();
            
            if (channel.isMuted(targetId)) {
                name = ChatColor.RED + name + ChatColor.WHITE;
            }
            else if (channel.isModerator(targetId)) {
                name = ChatColor.GREEN + name + ChatColor.WHITE;
            }
            if (i + 1 < count) {
                name += ", ";
            }
            
            // start a new line at 64 characters
            if (line.length() + name.length() > 64) {
                sender.sendMessage(line.toString().trim());
                line = new StringBuilder();
                i--;
            }
            else {
                line.append(name);
                if (i + 1 == count) {
                    sender.sendMessage(line.toString().trim());
                }
            }
        }
        
        return true;
    }
    
}
