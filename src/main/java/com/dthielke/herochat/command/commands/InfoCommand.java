/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCommand extends BasicCommand {
    
    public InfoCommand() {
        super("Info");
        setDescription(getMessage("command_info"));
        setUsage("/ch info " + ChatColor.DARK_GRAY + "[channel]");
        setArgumentRange(0, 1);
        setIdentifiers("ch info", "herochat info");
        setNotes(ChatColor.RED + "Note:" + ChatColor.YELLOW + " If no channel is given, your active",
            "      channel is used.");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        Channel channel;
        Chatter chatter = null;
        
        if (sender instanceof Player) {
            Player player = (Player) sender;
            chatter = Herochat.getChatterManager().getChatter(player);
        }
        
        if (args.length == 0) {
            if (chatter != null) {
                channel = chatter.getActiveChannel();
            }
            else {
                channel = Herochat.getChannelManager().getDefaultChannel();
            }
        }
        else {
            channel = Herochat.getChannelManager().getChannel(args[0]);
        }
        
        if (channel == null) {
            Messaging.send(sender, getMessage("info_noChannel"));
            return true;
        }
        
        if (chatter != null && chatter.canViewInfo(channel) != Chatter.Result.ALLOWED) {
            Messaging.send(sender, getMessage("info_noPermission"), channel.getColor() + channel.getName());
            return true;
        }
        
        sender.sendMessage(ChatColor.RED + "------------[ " + channel.getColor() + channel.getName() + ChatColor.RED +
            " ]------------");
        sender.sendMessage(ChatColor.YELLOW + "Name: " + ChatColor.RESET + channel.getName());
        sender.sendMessage(ChatColor.YELLOW + "Nick: " + ChatColor.RESET + channel.getNick());
        sender.sendMessage(ChatColor.YELLOW + "Format: " + ChatColor.RESET + channel.getFormat());
        if (channel.getPassword() != null)
            sender.sendMessage(ChatColor.YELLOW + "Password: " + ChatColor.RESET + channel.getPassword());
        if (channel.getDistance() > 0)
            sender.sendMessage(ChatColor.YELLOW + "Distance: " + ChatColor.RESET + channel.getDistance());
        sender.sendMessage(ChatColor.YELLOW + "Shortcut Allowed: " + ChatColor.RESET + channel.isShortcutAllowed());
        
        return true;
    }
    
}
