/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class MuteCommand extends BasicCommand {
    
    public MuteCommand() {
        super("Mute");
        setDescription(getMessage("command_mute"));
        setUsage("/ch mute §8[channel] <player>");
        setArgumentRange(1, 2);
        setIdentifiers("ch mute", "herochat mute");
        setNotes("\u00a7cNote:\u00a7e If no channel is given, user is", "      globally muted.");
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        Channel channel;
        Chatter chatter = null;
        
        if (sender instanceof Player) {
            Player player = (Player) sender;
            chatter = Herochat.getChatterManager().getChatter(player);
        }
        
        String targetName = args[args.length - 1];
        OfflinePlayer targetOfflinePlayer = Bukkit.getServer().getOfflinePlayer(targetName);
        Player targetPlayer = null;
        UUID targetId;
        if (targetOfflinePlayer != null) {
            targetId = targetOfflinePlayer.getUniqueId();
        }
        else {
            return false;
        }
        
        if (targetOfflinePlayer.isOnline()) {
            targetPlayer = Bukkit.getPlayer(targetId);
        }
        
        if (args.length == 2) {
            channel = Herochat.getChannelManager().getChannel(args[0]);
            if (channel == null) {
                Messaging.send(sender, getMessage("mute_noChannel"));
                return true;
            }
            
            if (chatter != null && chatter.canMute(channel) != Result.ALLOWED) {
                Messaging.send(sender, getMessage("mute_noPermission"));
                return true;
            }
            
            if (channel.isMuted(targetId)) {
                channel.setMuted(targetId, false);
                Messaging.send(sender, getMessage("mute_confirmUnmute"), targetName,
                    channel.getColor() + channel.getName());
                if (targetPlayer != null) {
                    Messaging.send(targetPlayer, getMessage("mute_notifyUnmute"),
                        channel.getColor() + channel.getName());
                }
            }
            else {
                channel.setMuted(targetId, true);
                Messaging.send(sender, getMessage("mute_confirmMute"), targetName,
                    channel.getColor() + channel.getName());
                if (targetPlayer != null) {
                    Messaging.send(targetPlayer, getMessage("mute_notifyMute"),
                        channel.getColor() + channel.getName());
                }
            }
        }
        else {
            if (chatter != null && !chatter.getPlayer().hasPermission("herochat.mute")) {
                Messaging.send(sender, getMessage("mute_noPermission"));
                return true;
            }
            
            if (targetPlayer == null) {
                Messaging.send(sender, getMessage("mute_noPlayer"));
                return true;
            }
            
            Chatter targetChatter = Herochat.getChatterManager().getChatter(targetPlayer);
            if (targetChatter.isMuted()) {
                targetChatter.setMuted(false, true);
                Messaging.send(sender, getMessage("mute_confirmGlobalUnmute"), targetPlayer.getName());
                Messaging.send(targetPlayer, getMessage("mute_notifyGlobalUnmute"));
            }
            else {
                targetChatter.setMuted(true, true);
                Messaging.send(sender, getMessage("mute_confirmGlobalMute"), targetPlayer.getName());
                Messaging.send(targetPlayer, getMessage("mute_notifyGlobalMute"));
            }
        }
        
        return true;
    }
    
}
