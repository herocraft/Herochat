/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command.commands;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.ChannelManager;
import com.dthielke.herochat.Chatter;
import com.dthielke.herochat.Chatter.Result;
import com.dthielke.herochat.Herochat;
import com.dthielke.herochat.command.BasicCommand;
import com.dthielke.herochat.util.Messaging;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand extends BasicCommand {
    
    public LeaveCommand() {
        super("Leave");
        setDescription(getMessage("command_leave"));
        setUsage("/ch leave " + ChatColor.DARK_GRAY + "<channel>");
        setArgumentRange(1, 1);
        setIdentifiers("leave", "ch leave", "herochat leave");
    }
    
    @Override
    public boolean execute(CommandSender sender, String identifier, String[] args) {
        if (!(sender instanceof Player)) {
            return true;
        }
        Player player = (Player) sender;
        
        ChannelManager channelMan = Herochat.getChannelManager();
        Channel channel = channelMan.getChannel(args[0]);
        if (channel == null) {
            Messaging.send(sender, getMessage("leave_noChannel"));
            return true;
        }
        
        Chatter chatter = Herochat.getChatterManager().getChatter(player);
        Result result = chatter.canLeave(channel);
        switch (result) {
            case INVALID:
                Messaging.send(sender, getMessage("leave_badChannel"), channel.getColor() + channel.getName());
                return true;
            case NO_PERMISSION:
                Messaging.send(sender, getMessage("leave_noPermission"), channel.getColor() + channel.getName());
                return true;
            default:
        }
        
        int channelCount = chatter.getChannels().size();
        if (channelCount == 1) {
            Messaging.send(sender, getMessage("leave_lastChannel"));
            return true;
        }
        
        channel.removeMember(chatter, true, true);
        Messaging.send(player, getMessage("leave_confirm"), channel.getColor() + channel.getName());
        
        if (chatter.getActiveChannel() == null || chatter.getActiveChannel().equals(channel)) {
            Channel newFocus = chatter.getLastFocusableChannel();
            if (newFocus == null || newFocus.equals(channel)) {
                newFocus = channelMan.getDefaultChannel();
            }
            chatter.setActiveChannel(newFocus, true, true);
        }
        
        return true;
    }
    
}
