/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat.command;

import org.bukkit.command.CommandSender;

public interface Command {
    
    /**
     * Cancels interaction with a {@link CommandSender} if this command {@link #isInteractive()}.
     *
     * @param sender the command sender currently interacted with
     */
    void cancelInteraction(CommandSender sender);
    
    /**
     * Executes the command.
     *
     * @param sender the command sender
     * @param identifier the identifier with which the command was called
     * @param args the arguments
     * @return {@code true} if the command was called properly, else {@code false}
     */
    boolean execute(CommandSender sender, String identifier, String... args);
    
    /**
     * Returns the name of the command.
     *
     * @return the name
     */
    String getName();
    
    /**
     * Returns a short description of the command.
     *
     * @return a short description
     */
    String getDescription();
    
    /**
     * Returns all possible identifiers of this command.
     *
     * @return all identifiers
     */
    String[] getIdentifiers();
    
    /**
     * Returns the minimum number of arguments for this command.
     *
     * @return the minimum argument number
     */
    int getMinArguments();
    
    /**
     * Returns the maximum number of arguments for this command.
     *
     * @return the maximum argument number
     */
    int getMaxArguments();
    
    /**
     * Returns notes about the behavior of the command implementation.
     *
     * @return notes about the behavior
     */
    String[] getNotes();
    
    /**
     * Returns the permission required to run this command or {@code ""} if there is none needed.
     *
     * @return the permission required to run this command
     */
    String getPermission();
    
    /**
     * Returns the usage of the command.
     *
     * @return the usage of the command
     */
    String getUsage();
    
    boolean isIdentifier(CommandSender sender, String input);
    
    /**
     * Returns whether an interactive command is currently querying a sender for input.
     * <p>
     * This will always return {@code false} for commands which do not query players.
     *
     * @param executor the sender
     * @return whether the command is in progress
     */
    boolean isInProgress(CommandSender executor);
    
    /**
     * Returns whether the command is interactive.
     *
     * @return whether the command is interactive
     */
    boolean isInteractive();
    
    /**
     * Returns whether the command is to be shown in the help menu.
     * <p>
     * This feature can be used for hiding certain administrative or internal commands from the player.
     *
     * @return whether the command is to be shown in the help menu
     */
    boolean isShownOnHelpMenu();
    
}
