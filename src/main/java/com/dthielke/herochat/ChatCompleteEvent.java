package com.dthielke.herochat;

import com.dthielke.herochat.Channel;
import com.dthielke.herochat.Chatter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Called when chat has successfully completed on a server, for use with monitoring, or sending data through other
 * means.
 */
public class ChatCompleteEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    private final Chatter sender;
    private final Channel channel;
    private final String msg;
    
    public ChatCompleteEvent(Chatter sender, Channel channel, String msg) {
        this.sender = sender;
        this.channel = channel;
        this.msg = msg;
    }
    
    /**
     * @return the sender
     */
    public Chatter getSender() {
        return sender;
    }
    
    /**
     * @return the channel
     */
    public Channel getChannel() {
        return channel;
    }
    
    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
}
