/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import org.bukkit.Bukkit;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.PluginManager;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class ChannelManager implements MessageFormatSupplier {
    
    private final static String
        DEFAULT_STANDARD_FORMAT = "{color}[{nick}] &r{prefix}{sender}{suffix}{color}: {msg}",
        DEFAULT_EMOTE_FORMAT = "{color}[{nick}] * {sender} {msg}",
        DEFAULT_ANNOUNCE_FORMAT = "{color}[{nick}] {msg}",
        DEFAULT_CONVERSATION_FORMAT = "&d{convoaddress} {convopartner}&d: {msg}";
    
    private Map<String, Channel> channels = new HashMap<>();
    private Channel defaultChannel;
    private Map<Chatter.Permission, Permission> wildcardPermissions = new EnumMap<>(Chatter.Permission.class);
    private Set<Chatter.Permission> modPermissions = EnumSet.noneOf(Chatter.Permission.class);
    private Map<String, String> worldAliases = new HashMap<>();
    private ChannelStorage storage;
    private String standardFormat = DEFAULT_STANDARD_FORMAT;
    private String emoteFormat = DEFAULT_EMOTE_FORMAT;
    private String announceFormat = DEFAULT_ANNOUNCE_FORMAT;
    private String conversationFormat = DEFAULT_CONVERSATION_FORMAT;
    private boolean usingEmotes;
    private final Map<String, TagFormatter> globalTagFormatter = new HashMap<>();
    
    public ChannelManager() {
        registerChannelPermissions();
    }
    
    public void addChannel(Channel channel) {
        channels.put(channel.getName().toLowerCase(), channel);
        if (channel.getNick() != null)
            channels.put(channel.getNick().toLowerCase(), channel);
        
        if (!channel.isTransient()) {
            // add the channel to the wildcard permissions
            for (Chatter.Permission p : Chatter.Permission.values()) {
                Permission perm = wildcardPermissions.get(p);
                perm.getChildren().put(p.form(channel).toLowerCase(), true);
                perm.recalculatePermissibles();
            }
            
            // change the default permission behavior for permissions
            PluginManager pluginMan = Bukkit.getServer().getPluginManager();
            
            for (Chatter.Permission chatPerm : new Chatter.Permission[] {
                Chatter.Permission.FOCUS,
                Chatter.Permission.AUTOJOIN,
                Chatter.Permission.FORCE_JOIN,
                Chatter.Permission.FORCE_LEAVE}) {
                PermissionDefault def = chatPerm == Chatter.Permission.FOCUS?
                    PermissionDefault.TRUE : PermissionDefault.FALSE;
                try {
                    Permission perm = new Permission(chatPerm.form(channel).toLowerCase(), def);
                    pluginMan.addPermission(perm);
                } catch (IllegalArgumentException ignored) {}
            }
            
            // set the default channel if we don't have one yet
            if (defaultChannel == null) {
                defaultChannel = channel;
            }
            storage.addChannel(channel);
        }
    }
    
    public boolean addTag(String tag, TagFormatter formatter) {
        if (!globalTagFormatter.containsKey(tag)) {
            globalTagFormatter.put(tag, formatter);
            return true;
        }
        return false;
    }
    
    public TagFormatter getTagFormatter(String tag) {
        return globalTagFormatter.get(tag);
    }
    
    /**
     * Enables a given chatter permission for moderators.
     *
     * @param permission the permission
     */
    public void addModPermission(Chatter.Permission permission) {
        modPermissions.add(permission);
    }
    
    /**
     * Checks whether moderators have a given chatter permission.
     *
     * @param permission the permission
     * @return whether moderators have the permission
     */
    public boolean checkModPermission(Chatter.Permission permission) {
        return modPermissions.contains(permission);
    }
    
    public void addWorldAlias(@NotNull String world, @NotNull String alias) {
        this.worldAliases.put(world, alias);
    }
    
    /**
     * Returns the alias of the given world name or the world name itself if there is none.
     *
     * @param world the world
     * @return the world name or its alias
     */
    public String getWorldAlias(@NotNull String world) {
        return this.worldAliases.containsKey(world)? this.worldAliases.get(world) : world;
    }
    
    /**
     * Returns an immutable map of world names and their aliases.
     *
     * @return an immutable map of world names and their aliases
     */
    public Map<String, String> getWorldAliases() {
        return Collections.unmodifiableMap(worldAliases);
    }
    
    public void clear() {
        defaultChannel = null;
        
        for (Channel channel : new LinkedList<>(channels.values()))
            removeChannel(channel);
        
        modPermissions.clear();
        storage = null;
        standardFormat = DEFAULT_STANDARD_FORMAT;
        announceFormat = DEFAULT_ANNOUNCE_FORMAT;
        emoteFormat = DEFAULT_EMOTE_FORMAT;
        conversationFormat = DEFAULT_CONVERSATION_FORMAT;
    }
    
    public Channel getChannel(String identifier) {
        return channels.get(identifier.toLowerCase());
    }
    
    public List<Channel> getChannels() {
        List<Channel> list = new ArrayList<>();
        for (Channel channel : channels.values())
            if (!list.contains(channel))
                list.add(channel);
        return list;
    }
    
    @NotNull
    public String getConversationFormat() {
        return conversationFormat;
    }
    
    public Channel getDefaultChannel() {
        return defaultChannel;
    }
    
    public Set<Chatter.Permission> getModPermissions() {
        return modPermissions;
    }
    
    @NotNull
    public String getStandardFormat() {
        return standardFormat;
    }
    
    @NotNull
    public String getAnnounceFormat() {
        return announceFormat;
    }
    
    @NotNull
    public String getEmoteFormat() {
        return emoteFormat;
    }
    
    public ChannelStorage getStorage() {
        return storage;
    }
    
    public boolean isUsingEmotes() {
        return usingEmotes;
    }
    
    public boolean hasChannel(String identifier) {
        return channels.containsKey(identifier.toLowerCase());
    }
    
    public void loadChannels() {
        for (Channel channel : storage.loadChannels())
            addChannel(channel);
    }
    
    public void registerChannelPermissions() {
        // setup empty wildcard permissions
        for (Chatter.Permission p : Chatter.Permission.values()) {
            Permission perm = new Permission(p.formWildcard(), PermissionDefault.FALSE);
            Bukkit.getServer().getPluginManager().addPermission(perm);
            wildcardPermissions.put(p, perm);
        }
    }
    
    public void removeChannel(Channel channel) {
        channels.remove(channel.getName().toLowerCase());
        if (channel.getNick() != null)
            channels.remove(channel.getNick().toLowerCase());
        
        if (!channel.isTransient()) {
            // remove the channel from the wildcard permissions
            for (Chatter.Permission p : Chatter.Permission.values()) {
                Permission perm = wildcardPermissions.get(p);
                perm.getChildren().remove(p.form(channel).toLowerCase());
                perm.recalculatePermissibles();
            }
            
            // remove permissions
            PluginManager pm = Bukkit.getServer().getPluginManager();
            String focusPermission = Chatter.Permission.FOCUS.form(channel).toLowerCase();
            pm.removePermission(focusPermission);
            pm.removePermission(Chatter.Permission.AUTOJOIN.form(channel).toLowerCase());
            pm.removePermission(Chatter.Permission.FORCE_JOIN.form(channel).toLowerCase());
            pm.removePermission(Chatter.Permission.FORCE_LEAVE.form(channel).toLowerCase());
            
            storage.removeChannel(channel);
        }
    }
    
    // SETTERS
    
    public void setDefaultChannel(Channel channel) {
        defaultChannel = channel;
    }
    
    public void setModPermissions(Set<Chatter.Permission> modPermissions) {
        this.modPermissions = modPermissions;
    }
    
    public void setStorage(ChannelStorage storage) {
        this.storage = storage;
    }
    
    public void setUsingEmotes(boolean usingEmotes) {
        this.usingEmotes = usingEmotes;
    }
    
    // FORMAT SETTERS
    
    public void setConversationFormat(@NotNull String conversationFormat) {
        this.conversationFormat = conversationFormat;
    }
    
    public void setStandardFormat(@NotNull String standardFormat) {
        this.standardFormat = standardFormat;
    }
    
    public void setAnnounceFormat(@NotNull String announceFormat) {
        this.announceFormat = announceFormat;
    }
    
    public void setEmoteFormat(@NotNull String emoteFormat) {
        this.emoteFormat = emoteFormat;
    }
    
}
