/*
 * Copyright (c) 2012 David "DThielke" Thielke <dave.thielke@gmail.com>.
 * All rights reserved.
 */

package com.dthielke.herochat;

import java.util.Set;

public interface ChannelStorage {
    
    void addChannel(Channel channel);
    
    void flagUpdate(Channel channel);
    
    Channel load(String name);
    
    Set<Channel> loadChannels();
    
    void removeChannel(Channel channel);
    
    void update();
    
    void update(Channel channel);
    
}
